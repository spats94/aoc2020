import re
import numpy as np
import time
start_time = time.time()

def get_list_three_cups(current_cup, dict_cups, list_three_cups):
    list_three_cups.append(current_cup)
    if len(list_three_cups) < 5:
        current_cup = dict_cups[current_cup]
        get_list_three_cups(current_cup, dict_cups, list_three_cups)
    else:
        dict_cups[list_three_cups[0]] = list_three_cups[-1]
        list_three_cups.pop(0)
        list_three_cups.pop(-1)


def get_destination(destination, list_three_cups, len_list_cup):
    destination[0] -= 1
    if destination[0] <= 0:
        destination[0] = len_list_cup
    if destination[0] in list_three_cups:
        get_destination(destination, list_three_cups, len_list_cup)


def move_cups(list_three_cups, destination, dict_cups):
    dict_cups[list_three_cups[-1]] = dict_cups[destination]
    dict_cups[destination] = list_three_cups[0]


list_cups = list(np.array([1, 3, 7, 8, 2, 6, 4, 9, 5], dtype=np.int64))
for cup in np.arange(10, 1000001):
    list_cups.append(np.array([cup], dtype=np.int64)[0])
dict_cups = {}
for i, cup in enumerate(list_cups):
    ii = (i + 1) % len(list_cups)
    dict_cups[cup] = list_cups[ii]


current_cup = 1
i = 0
while i < 10000000:
    list_three_cups = []
    get_list_three_cups(current_cup, dict_cups, list_three_cups)
    destination = [current_cup]
    get_destination(destination, list_three_cups, len(list_cups))
    move_cups(list_three_cups, destination[0], dict_cups)
    current_cup = dict_cups[current_cup]
    i += 1
result = dict_cups[1] * dict_cups[dict_cups[1]]
print(result)
print("--- %s seconds ---" % (time.time() - start_time))
end = 1