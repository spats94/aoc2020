import re
import numpy as np


def get_list_three_cups(i_current_cup, list_cups, list_three_cups):
    i_current_cup = (i_current_cup + 1) % len(list_cups)
    list_three_cups.append(list_cups[i_current_cup])
    list_cups[i_current_cup % len(list_cups)] = 0
    if len(list_three_cups) < 3:
        get_list_three_cups(i_current_cup, list_cups, list_three_cups)


def get_destination(destination, list_three_cups, len_list_cup):
    destination[0] -= 1
    if destination[0] <= 0:
        destination[0] = len_list_cup
    if destination[0] in list_three_cups:
        get_destination(destination, list_three_cups, len_list_cup)
    else:
        return destination[0]

def move_cups(i_next_cup, list_three_cups, destination, list_cups):
    list_cups[i_next_cup % len(list_cups)] = list_cups[(i_next_cup + 3) % len(list_cups)]
    list_cups[(i_next_cup + 3) % len(list_cups)] = 0
    if list_cups[i_next_cup % len(list_cups)] != destination:
        i_next_cup = (i_next_cup + 1) % len(list_cups)
        move_cups(i_next_cup, list_three_cups, destination, list_cups)
    else:
        list_cups[(i_next_cup + 1) % len(list_cups)] = list_three_cups[0]
        list_cups[(i_next_cup + 2) % len(list_cups)] = list_three_cups[1]
        list_cups[(i_next_cup + 3) % len(list_cups)] = list_three_cups[2]


list_cups = [3, 8, 9, 1, 2, 5, 4, 6, 7]


i_current_cup = 0
while i_current_cup < 100:
    list_three_cups = []
    get_list_three_cups(i_current_cup, list_cups, list_three_cups)
    destination = [list_cups[i_current_cup % len(list_cups)]]
    get_destination(destination, list_three_cups, len(list_cups))
    move_cups(i_current_cup + 1, list_three_cups, destination[0], list_cups)
    i_current_cup += 1

# value_current_cup = get_value_current_cup(list_cups, i_current_cup)
end = 1
