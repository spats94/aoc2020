import re
import itertools

import numpy as np


def read_input_data():
    with open('data.txt') as f:
        my_lines = f.readlines()
    dict_rules = {}
    list_mess = []
    for line in my_lines:
        id_rule = re.findall(r'(\d+):', line)
        if re.search(r' (\d+|[|])', line):
            rule = re.findall(r' (\d+|[|])', line)
            list_rule = [list(y) for x, y in itertools.groupby(rule, lambda z: z == '|') if not x]
            dict_rules[id_rule[0]] = list_rule
        if re.search(r'"(.+)"', line):
            rule = re.findall(r'"(.+)"', line)
            list_rule = [list(y) for x, y in itertools.groupby(rule, lambda z: z == '|') if not x]
            dict_rules[id_rule[0]] = list_rule
        if re.search(r'^[ab]+$', line):
            list_mess.append(re.findall(r'[ab]', line))
    return dict_rules, list_mess


def checkRuleWithoutNum(list_rule):
    for row in list_rule:
        for col in row:
            if not (col == 'a' or col == 'b'):
                return False
    return True


def create_empty_node(graph_rules, id_rules, dict_rules):
    dict_content_rule = {'rules': dict_rules[id_rules],
                         'in': [],
                         'out': []}
    graph_rules[id_rules] = dict_content_rule


def create_graph_rules(dict_rules):
    graph_rules = {}
    for id_rules in dict_rules:
        if id_rules not in graph_rules:
            create_empty_node(graph_rules, id_rules, dict_rules)
        for rules in dict_rules[id_rules]:
            for rule in rules:
                if rule not in graph_rules[id_rules]['out'] and rule not in ['a', 'b']:
                    graph_rules[id_rules]['out'].append(rule)
                if rule not in graph_rules and rule not in ['a', 'b']:
                    create_empty_node(graph_rules, rule, dict_rules)
                if rule not in ['a', 'b']:
                    if id_rules not in graph_rules[rule]['in']:
                        graph_rules[rule]['in'].append(id_rules)
    return graph_rules


dict_rules, list_mess = read_input_data()
graph_rules = create_graph_rules(dict_rules)
stop = 1


def is_sublist(sublist, reflist):
    return ''.join(sublist) in ''.join(reflist)


def get_max_len_mess(list_mess):
    max_len = 0
    for mess in list_mess:
        len_mess = len(mess)
        if max_len < len_mess:
            max_len = len_mess
    return max_len


def check_new_rule(newRule, list_mess):
    for element in newRule:
        if element not in ['a', 'b']:
            return True
        else:
            for mess in list_mess:
                if set(newRule).issubset(mess):
                    return True
    return False


def update_rule(contain_rule, insert_rules, id_rules, new_contain_rules, list_mess):
    for insert_rule in insert_rules:
        old_rule = contain_rule.copy()
        i = old_rule.index(id_rules)
        newRule = old_rule[:i]
        newRule.extend(insert_rule)
        newRule.extend(old_rule[i + 1:])
        if check_new_rule(newRule, list_mess):
            new_contain_rules.append(newRule)
    contain_rule = new_contain_rules
    for rule in contain_rule:
        if id_rules in rule:
            new_contain_rules.remove(rule)
            update_rule(rule, insert_rules, id_rules, new_contain_rules, list_mess)
    test = 2


def update_rules(contain_rules, insert_rules, id_rules, list_mess, max_len):
    new_contain_rules = []
    for contain_rule in contain_rules:
        if id_rules not in contain_rule:
            new_contain_rules.append(contain_rule)
            continue
        update_rule(contain_rule, insert_rules, id_rules, new_contain_rules, list_mess)
    return new_contain_rules


def solve_graph(graph_rules):
    cycle_run = 1
    max_len = get_max_len_mess(list_mess)
    while cycle_run:
        cycle_run = 0
        for id_rules in graph_rules:
            if graph_rules[id_rules]['out'] or not graph_rules[id_rules]['in']:
                continue
            cycle_run = 1
            insert_rules = graph_rules[id_rules]['rules']
            for id_contain_rule in graph_rules[id_rules]['in']:
                contain_rule = graph_rules[id_contain_rule]['rules']
                graph_rules[id_contain_rule]['rules'] = update_rules(contain_rule, insert_rules, id_rules, list_mess, max_len)
            while graph_rules[id_rules]['in']:
                id_contain_rule = graph_rules[id_rules]['in'][0]
                graph_rules[id_contain_rule]['out'].remove(id_rules)
                graph_rules[id_rules]['in'].remove(id_contain_rule)
            test = 1
        if not graph_rules['0']['out']:
            break
    end = 1


def solve_graph1(graph_rules):
    cycle_run = 1
    max_len = get_max_len_mess(list_mess)
    while cycle_run:
        cycle_run = 0
        for id_rules in graph_rules:
            if graph_rules[id_rules]['out'] or not graph_rules[id_rules]['in']:
                continue
            cycle_run = 1
            insert_rules = graph_rules[id_rules]['rules']
            for id_contain_rule in graph_rules[id_rules]['in']:
                contain_rule = graph_rules[id_contain_rule]['rules']
                graph_rules[id_contain_rule]['rules'] = update_rules(contain_rule, insert_rules, id_rules, list_mess, max_len)
            while graph_rules[id_rules]['in']:
                id_contain_rule = graph_rules[id_rules]['in'][0]
                graph_rules[id_contain_rule]['out'].remove(id_rules)
                graph_rules[id_rules]['in'].remove(id_contain_rule)
            test = 1
        if not graph_rules['0']['out']:
            break
    end = 1


solve_graph(graph_rules)

new_list_rule0 = []
for mess in list_mess:
    if mess in graph_rules['0']['rules']:
        new_list_rule0.append(mess)
        test = 1
for id_node in graph_rules:
    if graph_rules[id_node]['out'] or graph_rules[id_node]['in']:
        print(id_node, ': in', graph_rules[id_node]['in'])
        print(id_node, ': out', graph_rules[id_node]['out'])


def clear_mess_list(rules0, list_mess):
    for rule in rules0:
        if rule in list_mess:
            list_mess.remove(rule)
        else:
            rules0.remove(rule)


def init_new_graph(graph_rules):
    global new_graph
    new_graph = {}
    new_graph['0'] = graph_rules['0']
    new_graph['42'] = graph_rules['42']
    new_graph['42']['in'] = ['8', '11']
    new_graph['31'] = graph_rules['31']
    new_graph['31']['in'] = ['11']
    new_graph['8'] = graph_rules['8']
    new_graph['8']['rules'] = [['42'], ['42', '42']]
    new_graph['8']['in'] = ['0']
    new_graph['8']['out'] = ['42']
    new_graph['11'] = graph_rules['11']
    new_graph['11']['rules'] = [['42', '31'], ['42', '42', '31', '31']]
    new_graph['11']['in'] = ['0']
    new_graph['11']['out'] = ['42', '31']
    return new_graph



trig = 1
len_list_start = len(list_mess)
while trig:
    new_graph = init_new_graph(graph_rules)
    solve_graph1(new_graph)
    clear_mess_list(new_graph['0']['rules'], list_mess)
    test = 1


print(len(new_list_rule0))
test = 1