import re
import itertools

import numpy as np


def read_input_data():
    with open('data.txt') as f:
        my_lines = f.readlines()
    dict_rules = {}
    list_mess = []
    for line in my_lines:
        id_rule = re.findall(r'(\d+):', line)
        if re.search(r' (\d+|[|])', line):
            rule = re.findall(r' (\d+|[|])', line)
            list_rule = [list(y) for x, y in itertools.groupby(rule, lambda z: z == '|') if not x]
            dict_rules[id_rule[0]] = list_rule
        if re.search(r'"(.+)"', line):
            rule = re.findall(r'"(.+)"', line)
            list_rule = [list(y) for x, y in itertools.groupby(rule, lambda z: z == '|') if not x]
            dict_rules[id_rule[0]] = list_rule
        if re.search(r'^[ab]+$', line):
            list_mess.append(re.findall(r'[ab]', line))
    return dict_rules, list_mess


dict_rules, list_mess = read_input_data()
test = 1

def checkRuleWithoutNum(list_rule):
    for row in list_rule:
        for col in row:
            if not (col == 'a' or col == 'b'):
                return False
    return True


def updateRule(rules, rule_id, rule_list):
    newListRules = []
    i = rules.index(rule_id)
    rules.remove(rule_id)
    for el in rule_list:
        newRule = rules[:i]
        newRule.extend(el)
        newRule.extend(rules[i:])
        newListRules.append(newRule)
    return newListRules



flagRun = 1
while flagRun == 1:
    flagRun = 0
    for id_dict in dict_rules:
        list_rule = dict_rules[id_dict]
        if not checkRuleWithoutNum(list_rule):
            continue
        newList = []
        for id_dict1 in dict_rules:
            newList = []
            for rules in dict_rules[id_dict1]:
                if id_dict in rules:
                    rules = updateRule(rules, id_dict, dict_rules[id_dict])
                    newList.extend(rules)
                else:
                    newList.append(rules)
            dict_rules[id_dict1] = newList
    if not checkRuleWithoutNum(dict_rules['0']):
        flagRun = 1
    test = 1



k = 0
list_rules = dict_rules['0']
for mess in list_mess:
    if mess in list_rules:
        k += 1
test = 1










test = 1


