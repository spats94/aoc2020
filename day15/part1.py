import numpy as np
import re

# mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
# mem[8] = 11
# mem[7] = 101
# mem[8] = 0

list_number = [12,1,16,3,11,0]
while len(list_number) < 30000001:
    number = list_number[-1]
    i = len(list_number) - 2
    while i >= 0:
        if list_number[i] == number:
            break
        i -= 1
    else:
        list_number.append(0)
        continue
    list_number.append(len(list_number) - 1 - i)
    stop = 1

print(list_number[30000000])
stop = 1