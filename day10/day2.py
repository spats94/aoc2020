import numpy as np


def readData():
    fname = 'data2.txt'
    dtype_data = [('num', np.int32)]
    data = np.fromregex(fname, r'(\d+)', dtype_data)['num']
    return data


def runProgram(data):
    i = 0
    while i < len(data) - 3:
        oper = data[i]
        pos1 = data[i + 1]
        pos2 = data[i + 2]
        pos = data[i + 3]
        if oper == 1:
            data[pos] = data[pos1] + data[pos2]
        elif oper == 2:
            data[pos] = data[pos1] * data[pos2]
        elif oper == 99:
            break
        else:
            print("ERROR")
        i = i + 4
    return data[0]

for i in np.arange(0, 100):
    for k in np.arange(0, 100):
        data = readData()
        data[1] = i
        data[2] = k
        data0 = runProgram(data)
        if 19690720 == data0:
            print(100 * i + k)
            test = 1

