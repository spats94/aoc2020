import re
import numpy as np

fname = 'player1.txt'
dtype_data = [('card', np.int64)]
list_card_player1 = np.fromregex(fname, r'(.+)', dtype_data)['card']
list_card_player1 = list(list_card_player1)

fname = 'player2.txt'
dtype_data = [('card', np.int64)]
list_card_player2 = np.fromregex(fname, r'(.+)', dtype_data)['card']
list_card_player2 = list(list_card_player2)

while len(list_card_player2) > 0 and len(list_card_player1) > 0:
    cards_on_table = [list_card_player1[0], list_card_player2[0]]
    list_card_player1.pop(0)
    list_card_player2.pop(0)
    if cards_on_table[0] > cards_on_table[1]:
        list_card_player1.append(cards_on_table[0])
        list_card_player1.append(cards_on_table[1])
    else:
        list_card_player2.append(cards_on_table[1])
        list_card_player2.append(cards_on_table[0])
    test = 1

sum = 0
if len(list_card_player1) > 0:
    for i, card in enumerate(list_card_player1):
        sum += card * (len(list_card_player1) - i)
else:
    for i, card in enumerate(list_card_player2):
        sum += card * (len(list_card_player2) - i)
print(sum)



end = 1