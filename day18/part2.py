import numpy as np
import re

# mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
# mem[8] = 11
# mem[7] = 101
# mem[8] = 0

array_number = np.zeros(30000000, dtype=np.int32)
array_number[12] = 0
array_number[1] = 1
array_number[16] = 2
array_number[3] = 3
array_number[11] = 4

ind_number = 5
number = 0
while ind_number < 30000000 - 1:
    pred_ind = array_number[number]
    if pred_ind == 0 and number == 12:
        pred_number = number
        number = ind_number - array_number[pred_number]
        array_number[pred_number] = ind_number
    elif not pred_ind == 0:
        pred_number = number
        number = ind_number - array_number[pred_number]
        array_number[pred_number] = ind_number
    else:
        array_number[number] = ind_number
        number = 0
    ind_number += 1

print(number)
stop = 1