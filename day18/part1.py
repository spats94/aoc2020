import re


def calculateExpression(expression):
    if '(' in expression:
        expression = doExpandExpression(expression)
    else:
        i_elem = 2
        while i_elem < len(expression):
            if expression[i_elem-1] == '+':
                result = int(expression[i_elem]) + int(expression[i_elem - 2])
                expression[:] = [x for i, x in enumerate(expression) if i not in [i_elem - 2, i_elem - 1, i_elem]]
                expression.insert(i_elem - 2, str(result))
                expression = calculateExpression(expression)
            i_elem += 2
        i_elem = 2
        while i_elem < len(expression):
            if expression[i_elem - 1] == '*':
                result = int(expression[i_elem]) * int(expression[i_elem - 2])
                expression[:] = [x for i, x in enumerate(expression) if i not in [i_elem - 2, i_elem - 1, i_elem]]
                expression.insert(i_elem - 2, str(result))
                expression = calculateExpression(expression)
            i_elem += 2
    if type(expression) == list:
        return expression[0]
    else:
        return expression


def doExpandExpression(expression):
    for i, elem in enumerate(expression):
        if elem == '(':
            ii = i + 1
            while ii < len(expression):
                if expression[ii] == ')':
                    resultInBracket = calculateExpression(expression[i+1:ii])
                    expressionNew = []
                    for iii, elem1 in enumerate(expression):
                        if iii < i or iii > ii:
                            expressionNew.append(elem1)
                    expressionNew.insert(i, resultInBracket)
                    expression = expressionNew
                    expression = doExpandExpression(expression)
                elif expression[ii] == '(':
                    break
                ii += 1
    return expression


with open('data.txt') as f:
    my_lines = f.readlines()
list_result = []
for line in my_lines:
    exp_parse = re.findall(r"(\d+|[+|-|*]|[(|)])", line)
    list_result.append(calculateExpression(exp_parse))
print(list_result)
sum_result = 0
for result in list_result:
    sum_result += int(result)
print(sum_result)
