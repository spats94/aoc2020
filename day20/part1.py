import re
import numpy as np

with open('data.txt') as f:
    my_lines = f.readlines()

dict_tile = {}
for line in my_lines:
    if re.match(r'Tile ', line):
        name_tile = int(re.findall(r'(\d+)', line)[0])
        dict_tile[name_tile] = [np.zeros((10, 10), dtype=np.int8), []]
        i_row = 0
    if re.match(r'[.|#]{10}', line):
        parse_row = re.findall(r'[.|#]', line)
        for i_col, char in enumerate(parse_row):
            if char == '#':
                dict_tile[name_tile][0][i_row, i_col] = 1
            else:
                dict_tile[name_tile][0][i_row, i_col] = 0
        i_row += 1


def rotate_matrix(m):
    return np.array([[m[j][i] for j in range(len(m))] for i in range(len(m[0]) - 1, -1, -1)])


def get_list_rotate_matrix(m):
    list_rotate_matrix = []

    m1 = m.copy()
    n = 0
    while n <= 3:
        list_rotate_matrix.append(m1)
        m1 = rotate_matrix(m1)
        n += 1

    m1 = np.fliplr(m.copy())
    n = 0
    while n <= 3:
        list_rotate_matrix.append(m1)
        m1 = rotate_matrix(m1)
        n += 1


    m1 = np.flipud(m.copy())
    n = 0
    while n <= 3:
        list_rotate_matrix.append(m1)
        m1 = rotate_matrix(m1)
        n += 1
    return list_rotate_matrix




for id_dict1 in dict_tile:
    for id_dict2 in dict_tile:
        if id_dict1 != id_dict2 and id_dict1 not in dict_tile[id_dict2][1]:
            matrix_compare = dict_tile[id_dict2][0].copy()
            # Проверяем сверху
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][0, :] == matrix_compare[9, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем справа
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 9] == matrix_compare[:, 0]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем снизу
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][9, :] == matrix_compare[0, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем слева
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 0] == matrix_compare[:, 9]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1

for id_dict1 in dict_tile:
    for id_dict2 in dict_tile:
        if id_dict1 != id_dict2 and id_dict1 not in dict_tile[id_dict2][1]:
            matrix_compare = np.fliplr(dict_tile[id_dict2][0].copy())
            # Проверяем сверху
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][0, :] == matrix_compare[9, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем справа
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 9] == matrix_compare[:, 0]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем снизу
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][9, :] == matrix_compare[0, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем слева
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 0] == matrix_compare[:, 9]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1

for id_dict1 in dict_tile:
    for id_dict2 in dict_tile:
        if id_dict1 != id_dict2 and id_dict1 not in dict_tile[id_dict2][1]:
            matrix_compare = np.flipud(dict_tile[id_dict2][0].copy())
            # Проверяем сверху
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][0, :] == matrix_compare[9, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем справа
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 9] == matrix_compare[:, 0]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем снизу
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][9, :] == matrix_compare[0, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем слева
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 0] == matrix_compare[:, 9]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1


result = 1
for tile in dict_tile:
    if len(dict_tile[tile][1]) <= 2:
        tile_left_up = tile
        result *= tile
print(result)
end = 1


def find_up_left(dict_tile):
    list_rotate_matrix1 = get_list_rotate_matrix(dict_tile[2287][0])
    tile1 = dict_tile[2287][1][0]
    tile2 = dict_tile[2287][1][1]
    list_rotate_matrix2 = get_list_rotate_matrix(dict_tile[tile1][0])
    list_rotate_matrix3 = get_list_rotate_matrix(dict_tile[tile2][0])
    for rotate_matrix1 in list_rotate_matrix1:
        for rotate_matrix2 in list_rotate_matrix2:
            for rotate_matrix3 in list_rotate_matrix3:
                if all(rotate_matrix1[:, 9] == rotate_matrix2[:, 0]) and all(rotate_matrix1[9, :] == rotate_matrix3[0, :]):
                    dict_tile[2287][0] = rotate_matrix1
                    return rotate_matrix1
    else:
        for rotate_matrix1 in list_rotate_matrix1:
            for rotate_matrix2 in list_rotate_matrix2:
                for rotate_matrix3 in list_rotate_matrix3:
                    if all(rotate_matrix1[:, 9] == rotate_matrix3[:, 0]) and all(rotate_matrix1[9, :] == rotate_matrix2[0, :]):
                        dict_tile[2287][0] = rotate_matrix1
                        return rotate_matrix1


def find_matrix_right(dict_tile, dict_tile_matrix_left):
    tiles_matrix_right = dict_tile[dict_tile_matrix_left][1]
    for tile_matrix_right in tiles_matrix_right:
        list_rotate_matrix = get_list_rotate_matrix(dict_tile[tile_matrix_right][0])
        for rotate_matrix in list_rotate_matrix:
            if all(dict_tile[dict_tile_matrix_left][0][:, 9] == rotate_matrix[:, 0]):
                dict_tile[dict_tile_matrix_left][1].remove(tile_matrix_right)
                dict_tile[tile_matrix_right][1].remove(dict_tile_matrix_left)
                dict_tile[tile_matrix_right][0] = rotate_matrix
                return tile_matrix_right, rotate_matrix


def find_matrix_up(dict_tile, tile_matrix_up):
    tiles_matrix_down = dict_tile[tile_matrix_up][1]
    for tile_matrix_down in tiles_matrix_down:
        list_rotate_matrix = get_list_rotate_matrix(dict_tile[tile_matrix_down][0])
        for rotate_matrix in list_rotate_matrix:
            if all(dict_tile[tile_matrix_up][0][9, :] == rotate_matrix[0, :]):
                dict_tile[tile_matrix_up][1].remove(tile_matrix_down)
                dict_tile[tile_matrix_down][1].remove(tile_matrix_up)
                dict_tile[tile_matrix_down][0] = rotate_matrix
                return tile_matrix_down, rotate_matrix


test = 1
up_left_matrix1 = [tile_left_up, find_up_left(dict_tile)]
image = [[0] * 12 for i in range(12)]
image[0][0] = up_left_matrix1

for i_row in np.arange(12):
    for i_col in np.arange(12):
        if i_row == 0 and i_col == 0:
            continue
        if i_col != 0:
            image[i_row][i_col] = find_matrix_right(dict_tile, image[i_row][i_col - 1][0])
        else:
            image[i_row][i_col] = find_matrix_up(dict_tile, image[i_row - 1][i_col][0])

image_full = np.zeros((100, 100))
for row, row_image in enumerate(image):
    for col, col_image in enumerate(row_image):
        test_image = col_image[1][1:-1, 1:-1]
        image_full[row * 8:row * 8 + 8, col * 8:col * 8 + 8] = test_image[:, :]


with open('чудовище.txt') as f:
    my_lines = f.readlines()
array_monster = np.zeros((3, 20), dtype=np.int8)
dict_tile = {}
i_row = 0
for line in my_lines:
    if re.match(r'[ |#]', line):
        parse_row = re.findall(r'[ |#]', line)
        for i_col, char in enumerate(parse_row):
            if char == '#':
                array_monster[i_row, i_col] = 1
            else:
                array_monster[i_row, i_col] = 0
        i_row += 1


def find_monster(array_image, array_monster):
    for row in np.arange(3):
        for col in np.arange(20):
            if array_monster[row, col] == 1 and array_image[row, col] == 0:
                return False
    else:
        return True

def define_monster(array_image, array_monster):
    for row in np.arange(3):
        for col in np.arange(20):
            if array_monster[row, col] == 1:
                array_image[row, col] = 2
    return array_image


list_rotate_image = get_list_rotate_matrix(image_full)
flag = 0
for rotate_image in list_rotate_image:
    for i_row in np.arange(rotate_image.shape[0] - 3):
        for i_col in np.arange(rotate_image.shape[1] - 20):
            if find_monster(rotate_image[i_row:i_row+3, i_col:i_col+20], array_monster):
                rotate_image[i_row:i_row + 3, i_col:i_col + 20] = define_monster(rotate_image[i_row:i_row+3, i_col:i_col+20], array_monster)
                flag = 1
    if flag == 1:
        sum1 = 0
        for i_row in np.arange(rotate_image.shape[0]):
            for i_col in np.arange(rotate_image.shape[1]):
                if rotate_image[i_row, i_col] == 1:
                    sum1 += 1
        break
print(sum1)
test = 1