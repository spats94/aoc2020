import re
import numpy as np

with open('data.txt') as f:
    my_lines = f.readlines()

dict_tile = {}
for line in my_lines:
    if re.match(r'Tile ', line):
        name_tile = int(re.findall(r'(\d+)', line)[0])
        dict_tile[name_tile] = [np.zeros((10, 10), dtype=np.int8), []]
        i_row = 0
    if re.match(r'[.|#]{10}', line):
        parse_row = re.findall(r'[.|#]', line)
        for i_col, char in enumerate(parse_row):
            if char == '#':
                dict_tile[name_tile][0][i_row, i_col] = 1
            else:
                dict_tile[name_tile][0][i_row, i_col] = 0
        i_row += 1


def rotate_matrix(m):
    return np.array([[m[j][i] for j in range(len(m))] for i in range(len(m[0]) - 1, -1, -1)])


for id_dict1 in dict_tile:
    for id_dict2 in dict_tile:
        if id_dict1 != id_dict2 and id_dict1 not in dict_tile[id_dict2][1]:
            matrix_compare = dict_tile[id_dict2][0].copy()
            # Проверяем сверху
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][0, :] == matrix_compare[9, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем справа
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 9] == matrix_compare[:, 0]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем снизу
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][9, :] == matrix_compare[0, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем слева
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 0] == matrix_compare[:, 9]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1

for id_dict1 in dict_tile:
    for id_dict2 in dict_tile:
        if id_dict1 != id_dict2 and id_dict1 not in dict_tile[id_dict2][1]:
            matrix_compare = np.fliplr(dict_tile[id_dict2][0].copy())
            # Проверяем сверху
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][0, :] == matrix_compare[9, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем справа
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 9] == matrix_compare[:, 0]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем снизу
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][9, :] == matrix_compare[0, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем слева
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 0] == matrix_compare[:, 9]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1

for id_dict1 in dict_tile:
    for id_dict2 in dict_tile:
        if id_dict1 != id_dict2 and id_dict1 not in dict_tile[id_dict2][1]:
            matrix_compare = np.flipud(dict_tile[id_dict2][0].copy())
            # Проверяем сверху
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][0, :] == matrix_compare[9, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем справа
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 9] == matrix_compare[:, 0]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем снизу
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][9, :] == matrix_compare[0, :]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1
            # Проверяем слева
            n = 0
            while n <= 3:
                if all(dict_tile[id_dict1][0][:, 0] == matrix_compare[:, 9]):
                    dict_tile[id_dict1][1].append(id_dict2)
                    dict_tile[id_dict2][1].append(id_dict1)
                n += 1
                matrix_compare = rotate_matrix(matrix_compare)
            test = 1


result = 1
for tile in dict_tile:
    if len(dict_tile[tile][1]) <= 2:
        result *= tile
print(result)
end = 1


