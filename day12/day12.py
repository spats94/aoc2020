import numpy as np
import re

fname = 'data.txt'
dtype_data = [('instruction', '<U100')]
list_instructions = np.fromregex(fname, r'(.+)', dtype_data)['instruction']


def doInstruction(instruction, direction, now_pos):
    command, value = re.findall(r'(\w)(\d+)', instruction)[0]
    value = int(value)
    directions = [np.array([-1, 0]),
                  np.array([0, -1]),
                  np.array([1, 0]),
                  np.array([0, 1])]
    if command == 'N':
        now_pos = np.array([0, -1]) * value + now_pos
    elif command == 'W':
        now_pos = np.array([-1, 0]) * value + now_pos
    elif command == 'S':
        now_pos = np.array([0, 1]) * value + now_pos
    elif command == 'E':
        now_pos = np.array([1, 0]) * value + now_pos
    elif command == 'F':
        now_pos = direction * value + now_pos
    elif command == 'R' or command == 'L':
        n_povorot = int(value / 90)
        for i_directions, dir_1 in enumerate(directions):
            if np.all(dir_1 == direction):
                break
        if command == 'R':
            i_new = (i_directions + n_povorot) % 4
            direction = directions[i_new]
        if command == 'L':
            i_new = (i_directions - n_povorot) % 4
            direction = directions[i_new]
    return now_pos, direction


direction = np.array([1, 0])
pos = np.array([0, 0])
for instruction in list_instructions:
    pos, direction = doInstruction(instruction, direction, pos)
print(np.abs(pos[0]) + np.abs(pos[1]))
end = 1
