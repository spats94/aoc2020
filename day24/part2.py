import copy
import re
import numpy as np


def getParseLine(line, parseLine):
    if line[0:2] == 'se':
        parseLine.append('se')
        line = line[2:]
    elif line[0:2] == 'sw':
        parseLine.append('sw')
        line = line[2:]
    elif line[0:2] == 'nw':
        parseLine.append('nw')
        line = line[2:]
    elif line[0:2] == 'ne':
        parseLine.append('ne')
        line = line[2:]
    elif line[0:1] == 'e':
        parseLine.append('e')
        line = line[1:]
    elif line[0:1] == 'w':
        parseLine.append('w')
        line = line[1:]
    if len(line) > 0:
        getParseLine(line, parseLine)


def getCoord(parseLine, coord, dict_direction):
    if len(parseLine) > 0:
        direction = dict_direction[parseLine[0]]
        coord += direction
        parseLine.pop(0)
        getCoord(parseLine, coord, dict_direction)


def switchTile(dict_tiles, coord):
    try:
        dict_tiles[coord]['isBlack'] = not dict_tiles[coord]['isBlack']
        test = 1
    except:
        dict_tiles[coord] = {'isBlack': True,
                             'neighbors': []}


def createDictTiles(my_lines, dict_tiles, dict_direction):
    line = my_lines[0]
    my_lines.pop(0)
    parseLine = []
    getParseLine(re.findall(r"(.+)", line)[0], parseLine)
    coord = np.array([0, 0, 0])
    getCoord(parseLine, coord, dict_direction)
    switchTile(dict_tiles, tuple(coord))
    if len(my_lines) > 0:
        createDictTiles(my_lines, dict_tiles, dict_direction)


with open('data.txt') as f:
    my_lines = f.readlines()

dict_tiles = {}
dict_direction = {'w': np.array([0, 1, -1]),
                  'e': np.array([0, -1, 1]),
                  'nw': np.array([1, 0, -1]),
                  'se': np.array([-1, 0, 1]),
                  'sw': np.array([-1, 1, 0]),
                  'ne': np.array([1, -1, 0])}

createDictTiles(my_lines, dict_tiles, dict_direction)


def createConnections(id_dict, list_direction, new_dict_tiles):
    for direction in list_direction:
        id_dict_neighbor = tuple(np.array(id_dict) + direction)
        try:
            new_dict_tiles[id_dict_neighbor]['neighbors'].append(id_dict)
        except:
            new_dict_tiles[id_dict_neighbor] = {'isBlack': False,
                                                'neighbors': [id_dict]}
    return new_dict_tiles


def destroyConnections(id_dict, list_direction, new_dict_tiles):
    for direction in list_direction:
        id_dict_neighbor = tuple(np.array(id_dict) + direction)
        try:
            new_dict_tiles[id_dict_neighbor]['neighbors'].remove(id_dict)
        except:
            continue
    return new_dict_tiles


def createDay0(dict_tiles, list_direction):
    new_dict_tiles = copy.deepcopy(dict_tiles)
    for id_dict in dict_tiles:
        if dict_tiles[id_dict]['isBlack']:
            createConnections(id_dict, list_direction, new_dict_tiles)
    dict_tiles = new_dict_tiles
    return dict_tiles


def doChangeOneDay(dict_tiles, list_direction):
    new_dict_tiles = copy.deepcopy(dict_tiles)
    for id_dict in dict_tiles:
        if dict_tiles[id_dict]['isBlack'] and not 0 < len(dict_tiles[id_dict]['neighbors']) < 3:
            new_dict_tiles[id_dict]['isBlack'] = False
            new_dict_tiles = destroyConnections(id_dict, list_direction, new_dict_tiles)
        elif not dict_tiles[id_dict]['isBlack'] and len(dict_tiles[id_dict]['neighbors']) == 2:
            new_dict_tiles[id_dict]['isBlack'] = True
            new_dict_tiles = createConnections(id_dict, list_direction, new_dict_tiles)
    dict_tiles = new_dict_tiles
    return dict_tiles


n_black_tiles = 0
for id_dict in dict_tiles:
    n_black_tiles += dict_tiles[id_dict]['isBlack']
print(n_black_tiles)

dict_tiles = createDay0(dict_tiles, list(dict_direction.values()))
i = 1
while i <= 100:
    dict_tiles = doChangeOneDay(dict_tiles, list(dict_direction.values()))
    n_black_tiles = 0
    for id_dict in dict_tiles:
        n_black_tiles += dict_tiles[id_dict]['isBlack']
    print('day', i, ':', n_black_tiles)
    i += 1


# value_current_cup = get_value_current_cup(list_cups, i_current_cup)
end = 1
