import re
import numpy as np


def getParseLine(line, parseLine):
    if line[0:2] == 'se':
        parseLine.append('se')
        line = line[2:]
    elif line[0:2] == 'sw':
        parseLine.append('sw')
        line = line[2:]
    elif line[0:2] == 'nw':
        parseLine.append('nw')
        line = line[2:]
    elif line[0:2] == 'ne':
        parseLine.append('ne')
        line = line[2:]
    elif line[0:1] == 'e':
        parseLine.append('e')
        line = line[1:]
    elif line[0:1] == 'w':
        parseLine.append('w')
        line = line[1:]
    if len(line) > 0:
        getParseLine(line, parseLine)


def getCoord(parseLine, coord, dict_direction):
    if len(parseLine) > 0:
        direction = dict_direction[parseLine[0]]
        coord += direction
        parseLine.pop(0)
        getCoord(parseLine, coord, dict_direction)


def switchTile(dict_tiles, coord):
    try:
        dict_tiles[coord] = not dict_tiles[coord]
    except:
        dict_tiles[coord] = True


def createDictTiles(my_lines, dict_tiles, dict_direction):
    line = my_lines[0]
    my_lines.pop(0)
    parseLine = []
    getParseLine(re.findall(r"(.+)", line)[0], parseLine)
    coord = np.array([0, 0, 0])
    getCoord(parseLine, coord, dict_direction)
    switchTile(dict_tiles, tuple(coord))
    if len(my_lines) > 0:
        createDictTiles(my_lines, dict_tiles, dict_direction)


with open('data.txt') as f:
    my_lines = f.readlines()

dict_tiles = {}
dict_direction = {'w': np.array([0, 1, -1]),
                  'e': np.array([0, -1, 1]),
                  'nw': np.array([1, 0, -1]),
                  'se': np.array([-1, 0, 1]),
                  'sw': np.array([-1, 1, 0]),
                  'ne': np.array([1, -1, 0])}

createDictTiles(my_lines, dict_tiles, dict_direction)

n_black_tiles = 0
for id_dict in dict_tiles:
    n_black_tiles += dict_tiles[id_dict]

# value_current_cup = get_value_current_cup(list_cups, i_current_cup)
end = 1
