import numpy as np
import re

fname = 'data.txt'
dtype_data = [('instruction', '<U100')]
list_instructions = np.fromregex(fname, r'(.+)', dtype_data)['instruction']


def doInstruction(instruction, now_pos, target_point):
    command, value = re.findall(r'(\w)(\d+)', instruction)[0]
    value = int(value)
    if command == 'N':
        target_point = np.array([0, -1]) * value + target_point
    elif command == 'W':
        target_point = np.array([-1, 0]) * value + target_point
    elif command == 'S':
        target_point = np.array([0, 1]) * value + target_point
    elif command == 'E':
        target_point = np.array([1, 0]) * value + target_point
    elif command == 'F':
        now_pos = target_point * value + now_pos
    elif command == 'R' or command == 'L':
        n_povorot = int(value / 90)
        if command == 'R':
            for k in np.arange(0, n_povorot):
                target_point = np.array([-target_point[1], target_point[0]])
        if command == 'L':
            for k in np.arange(0, n_povorot):
                target_point = np.array([target_point[1], -target_point[0]])

    return now_pos, target_point


target_point = np.array([10, -1])
pos = np.array([0, 0])
for instruction in list_instructions:
    pos, target_point = doInstruction(instruction, pos, target_point)
print(np.abs(pos[0]) + np.abs(pos[1]))
end = 1
