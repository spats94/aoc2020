import numpy as np
import re

fname = 'data.txt'
dtype_data = [('instruction', '<U100')]
file1 = open(fname, 'r')
time_treb = int(file1.readline().replace('\n', ''))
buses = file1.readline().split(',')


from functools import reduce
from operator import mul


def chinese_remainder(n, a):
    """
    Chinese Remainder Theorem.

    :param n: list of pairwise relatively prime integers
    :param a: remainders when x is divided by n
    """
    s = 0
    prod = reduce(mul, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        s += a_i * inverse(p, n_i) * p
    return s % prod


def inverse(a, b):
    """
    Modular multiplicative inverse.
    """
    b0 = b
    x0, x1 = 0, 1
    if b == 1:
        return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0:
        x1 += b0
    return x1


n = [29, 41, 37, 653, 13, 17, 23, 823, 19]
a = [0, 41 - 19, 37 - 23, 653 - 29, 52 - 42, 51 - 46, 69 - 52, 823 - 60, 95 - 79]
print(chinese_remainder(n, a))


for bus in buses:
    if not bus == 'x':
        bus = int(bus)
        time_wait = bus - (time_treb % bus)
        if time_wait < min_time_wait:
            best_bus = bus
            min_time_wait = time_wait

print(best_bus * min_time_wait)
end = 1


def doInstruction(instruction, direction, now_pos):
    command, value = re.findall(r'(\w)(\d+)', instruction)[0]
    value = int(value)
    directions = [np.array([-1, 0]),
                  np.array([0, -1]),
                  np.array([1, 0]),
                  np.array([0, 1])]
    if command == 'N':
        now_pos = np.array([0, -1]) * value + now_pos
    elif command == 'W':
        now_pos = np.array([-1, 0]) * value + now_pos
    elif command == 'S':
        now_pos = np.array([0, 1]) * value + now_pos
    elif command == 'E':
        now_pos = np.array([1, 0]) * value + now_pos
    elif command == 'F':
        now_pos = direction * value + now_pos
    elif command == 'R' or command == 'L':
        n_povorot = int(value / 90)
        for i_directions, dir_1 in enumerate(directions):
            if np.all(dir_1 == direction):
                break
        if command == 'R':
            i_new = (i_directions + n_povorot) % 4
            direction = directions[i_new]
        if command == 'L':
            i_new = (i_directions - n_povorot) % 4
            direction = directions[i_new]
    return now_pos, direction


direction = np.array([1, 0])
pos = np.array([0, 0])
for instruction in list_instructions:
    pos, direction = doInstruction(instruction, direction, pos)
print(np.abs(pos[0]) + np.abs(pos[1]))
end = 1
