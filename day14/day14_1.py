import numpy as np
import re

# mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
# mem[8] = 11
# mem[7] = 101
# mem[8] = 0
fname = 'data.txt'
with open(fname) as f:
    my_lines = f.readlines()
list_data = []
mask = ""
for line in my_lines:
    if re.match(r'mask = ', line):
        if not len(mask) == 0:
            list_data.append([mask, mem_list])
        mask = re.findall(r'[X|1|0]+', line)[0]
        mem_list = []
    if re.match(r'mem', line):
        mem = re.findall(r'(\d+)', line)
        mem_list.append([int(mem[0]), np.binary_repr(int(mem[1]), 36)])
        test = 1
    if re.match(r'^\n$', line):
        list_data.append([mask, mem_list])
        break

memory_list = np.zeros(100000, dtype=np.int64)
for data in list_data:
    mask = data[0]
    for mem in data[1]:
        for i, char in enumerate(mask):
            if char == '0':
                mem[1] = mem[1][:i] + '0' + mem[1][i + 1:]
            if char == '1':
                mem[1] = mem[1][:i] + '1' + mem[1][i + 1:]
        test = 1
        memory_list[mem[0]] = int(mem[1], 2)
    test = 1
    pass
print(np.sum(memory_list))
end = 1