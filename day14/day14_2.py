import numpy as np
import re

fname = 'data.txt'
with open(fname) as f:
    my_lines = f.readlines()
list_data = []
mask = ""
for line in my_lines:
    if re.match(r'mask = ', line):
        if not len(mask) == 0:
            list_data.append([mask, mem_list])
        mask = re.findall(r'[X|1|0]+', line)[0]
        mem_list = []
    if re.match(r'mem', line):
        mem = re.findall(r'(\d+)', line)
        mem_list.append([int(mem[0]), np.binary_repr(int(mem[1]), 36)])
        test = 1
    if re.match(r'^\n$', line):
        list_data.append([mask, mem_list])
        break


def getIndUndefinedBit(mask):
    ind = []
    for i, char in enumerate(mask):
        if char == 'X':
            ind.append(i)
    return ind


def getResultMask(mask, address):
    address_binary = np.binary_repr(address, 36)
    result_mask = np.binary_repr(0, 36)
    for i, char in enumerate(mask):
        if char == '0':
            result_mask = result_mask[:i] + address_binary[i] + result_mask[i + 1:]
        elif char == '1':
            result_mask = result_mask[:i] + '1' + result_mask[i + 1:]
        else:
            result_mask = result_mask[:i] + 'X' + result_mask[i + 1:]
    return result_mask



def getChangedAddress(mask):
    array_changed_address_binary = np.array(mask.replace('X', '0'))
    indUndefinedBit = getIndUndefinedBit(mask)
    for i_char in indUndefinedBit:
        new_array = np.r_[array_changed_address_binary, array_changed_address_binary]
        i = 0
        while i < int(len(new_array) / 2):
            new_array[i] = new_array[i][:i_char] + '0' + new_array[i][i_char + 1:]
            i += 1
        while i < int(len(new_array)):
            new_array[i] = new_array[i][:i_char] + '1' + new_array[i][i_char + 1:]
            i += 1
        array_changed_address_binary = new_array
    array_changed_address = np.zeros(len(array_changed_address_binary), dtype=np.int64)
    for i, changed_address in enumerate(array_changed_address_binary):
        array_changed_address[i] = int(changed_address, 2)
    return array_changed_address


def addInMemoryChangedAddress(dict_memory, array_changed_address, value):
    test = np.max(array_changed_address)
    for changed_address in array_changed_address:
        dict_memory[changed_address] = value
    return dict_memory


dict_memory = {}
for data in list_data:
    mask = data[0]
    for mem in data[1]:
        result_mask = getResultMask(mask, mem[0])
        list_changed_address = getChangedAddress(result_mask)
        array_memory = addInMemoryChangedAddress(dict_memory, list_changed_address, int(mem[1], 2))
    pass
sum = 0
for id_dict in dict_memory:
    sum += dict_memory[id_dict]
print(sum)
end = 1
