import re

with open('data.txt') as f:
    my_lines = f.readlines()
list_result = []
dict_allergens = {}
dict_ingredients = {}
# Создаем словарь с аллегренами
for line in my_lines:
    line_parse = re.findall(r"(\w+)", line)
    flag = 0
    list_ingredients = []
    for word in line_parse:
        if word == 'contains':
            flag = 1
            continue
        if flag == 0:
            try:
                dict_ingredients[word] += 1
            except:
                dict_ingredients[word] = 1
            list_ingredients.append(word)
        else:
            try:
                dict_allergens[word].append(list_ingredients)
            except:
                dict_allergens[word] = [list_ingredients]
# ищем пересечения аллергенов
for allergens in dict_allergens:
    lists_allergens = dict_allergens[allergens]
    lists_allergens1 = lists_allergens[0]
    if len(lists_allergens) > 1:
        i = 1
        while i < len(lists_allergens):
            lists_allergens1 = list(set(lists_allergens1) & set(lists_allergens[i]))
            i += 1
    dict_allergens[allergens] = lists_allergens1
flag = 1
while flag == 1:
    flag = 0
    for allergens in dict_allergens:
        if len(dict_allergens[allergens]) == 1:
            allergen = dict_allergens[allergens][0]
            for allergens1 in dict_allergens:
                if len(dict_allergens[allergens1]) > 1:
                    try:
                        dict_allergens[allergens1].remove(allergen)
                    except:
                        pass
        else:
            flag = 1
for allergens in dict_allergens:
    del dict_ingredients[dict_allergens[allergens][0]]
sum = 0
for ingredient in dict_ingredients:
    sum += dict_ingredients[ingredient]
print(sum)

list_keys = list(dict_allergens.keys())
list_keys.sort()
str_for_print = ""
for i in list_keys:
    str_for_print += dict_allergens[i][0] + ','
print(str_for_print[:-1])
end1 = 1


